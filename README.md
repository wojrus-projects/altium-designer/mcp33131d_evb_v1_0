# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do ADC Microchip MCP33131D-05-E/MS.

# Projekt PCB

Schemat: [doc/MCP33131D_EVB_V1_0_SCH.pdf](doc/MCP33131D_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/MCP33131D_EVB_V1_0_3D.pdf](doc/MCP33131D_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
